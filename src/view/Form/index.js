import {useEffect, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useDispatch, useSelector} from 'react-redux'
import {useParams} from 'react-router-dom'
import {FormGroup, Input, Label, Select, Textarea} from '../../component'
import {addCar, getCarById, updateCar} from '../../store/car/car'

import './style.css'
import {addCategory, updateCategory} from "../../store/category/category";
import {fileUpload} from "../../store/photo/photo";

const Form = ({setModal, data, type, param}) => {
    const dispatch = useDispatch()
    const {register, handleSubmit, reset} = useForm()

    const category = useSelector(state => state.category)
    const {data: photoData} = useSelector(state => state.photo)
    let marka = []
    category?.data?.forEach(item => marka.push({value: item._id, label: item.name}))

    const [value, setValue] = useState({});
    const [name, setName] = useState(null);

    const forms = [
        {
            label: 'Markasi',
            name: 'categoryId',
            options: marka,
            defaultValue: data?.marka?._id || param || '63180c53d0953487569045c7',
            select: true,
        },
        {
            label: 'Tanirovka',
            name: 'tonirovka',
            options: [
                {value: 'oldi orqa qilingan', label: 'Oldi orqa qilingan'},
                {value: 'yoq', label: "Yo'q"},
            ],
            defaultValue: data?.tonirovka ?? 'yoq',
            select: true,
        },
        {label: 'Motor', name: 'motor', placeholder: 'Kiriting', input: true, defaultValue: data?.motor},
        {label: 'Year', name: 'year', placeholder: 'Kiriting', input: true, defaultValue: data?.year},
        {label: 'Color', name: 'color', placeholder: 'Kiriting', input: true, defaultValue: data?.color},
        {label: 'Distance', name: 'distance', placeholder: 'Kiriting', input: true, defaultValue: data?.distance},
        {label: 'Gearbook', name: 'gearbok', placeholder: 'Kiriting', input: true, defaultValue: data?.gearbok},
        {label: 'Narxi', name: 'price', placeholder: 'Kiriting', input: true, defaultValue: data?.price},
        {
            label: 'Rasm 360 ichki makon',
            name: 'imgUrlInside',
            type: 'file',
            placeholder: 'Yuklash',
            input: true,
            // defaultValue:  data?.imgUrlInside
        },
        {
            label: 'Rasm 360 tashqi makon',
            name: 'imgUrlAutside',
            type: 'file',
            placeholder: 'Yuklash',
            input: true,
            file: true,
            // defaultValue:  data?.imgUrlAutside
        },
        {
            label: 'Description',
            name: 'description',
            placeholder: 'Mazmuni kiriting',
            textarea: true,
            cols: 30,
            rows: 8,
            defaultValue: data?.description
        },
        {
            label: 'Modeli turi uchun rasm',
            name: 'imgUrl',
            type: 'file',
            placeholder: 'Yuklash',
            input: true,
            file: true,
            // defaultValue:  data?.imgUrl
        },
    ]

    const forms2 = [
        {label: 'Name', name: 'name', placeholder: 'Kiriting', input: true, defaultValue: data?.name},
        {
            label: 'Rasm',
            name: 'imgUrl',
            type: 'file',
            placeholder: 'Yuklash',
            input: true,
            file: true
            // defaultValue:  data?.imgUrlInside
        },
    ]

    const onSubmit = async values => {
        if (type) {
            values.year = Number(values.year)
            values.price = Number(values.price)
            values.imgUrlInside = values['imgUrlInside'][0].name
        }
        Object.assign(values, value)

        console.log(values)
        // const formData = new FormData()
        // const objectkeys = Object.keys(values)
        // objectkeys.forEach(objectkey => {
        //     if (objectkey !== 'imgUrlInside' && objectkey !== 'imgUrlOutside' && objectkey !== 'imgUrl') {
        //         formData.append(objectkey, values[objectkey])
        //     }
        // })
        // formData.append('imgUrlInside', values['imgUrlInside'][0])
        // formData.append('imgUrlOutside', values['imgUrlOutside'][0])
        //
        // formData.append('imgUrl', values['imgUrl'][0])

        if (data) {
            values._id = data._id;
            if (type) {
                dispatch(updateCar(values))
            } else {
                dispatch(updateCategory(values))
            }
        } else {
            if (type) {
                dispatch(addCar(values))
            } else {
                dispatch(addCategory(values))
            }
        }
        reset()
    }

    const onChange = async (e, name) => {
        let formData = new FormData()
        formData.append('file', e?.target?.files[0])
        await dispatch(fileUpload(formData))
        setName(name)
    }
    useEffect(() => {
        if (name && photoData) {
            let newValue = {};
            console.log(photoData)
            newValue[name] = photoData
            setValue({...value, ...newValue})
            setName(null)
        }
    }, [photoData]);


    return (
        <div className='bg-white rounded p-1 m-auto'>
            <div className='d-flex'>
                <div className='header'>
                    <span/>
                    <h5>Mashina qo'shish</h5>
                </div>
                <i className='fa-solid fa-x' onClick={() => setModal(false)}/>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} encType='multipart/form-data'>
                <div className='grid-2'>
                    {(type ? forms : forms2).map((form, index) => (
                        <FormGroup className={`${index % 2 === 0 ? 'pr-1' : 'pl-1'} my-26`} key={index}>
                            <Label>{form.label}</Label>
                            {form.options && (
                                <Select
                                    {...register(form.name, {required: true})}
                                    options={form?.options}
                                    defaultValue={form.defaultValue}
                                />
                            )}
                            {form.textarea && (
                                <Textarea
                                    {...register(form.name, {required: true})}
                                    placeholder={form.placeholder}
                                    rows={form.rows}
                                    cols={form.cols}
                                    defaultValue={form.defaultValue}
                                />
                            )}
                            {form.file ?
                                <Input
                                    onChange={(e) => onChange(e, form.name)}
                                    type={form?.type}
                                    placeholder={form.placeholder}
                                />
                                : form.input &&
                                <Input
                                    {...register(form.name, {required: true})}
                                    type={form.type ?? 'text'}
                                    placeholder={form.placeholder}
                                    defaultValue={form.defaultValue}
                                />
                            }
                        </FormGroup>
                    ))}
                </div>
                <hr/>
                <button className='text-right' type='submit'>
                    Saqlash
                </button>
            </form>
        </div>
    )
}

export default Form
