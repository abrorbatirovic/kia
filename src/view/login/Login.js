import React from 'react';
import {loginStart, getProfile} from '../../store/auth/auth'
import {useSelector, useDispatch} from 'react-redux'
import {useEffect} from 'react'
import {useNavigate} from 'react-router-dom'

import './main.css'

const Login = () => {

    const dispatch = useDispatch()
    const history = useNavigate()
    const {loading,status, userStatus} = useSelector(state=>state.auth)

    debugger

    useEffect(() => {
        localStorage.clear()
    }, [])

    useEffect(() => {
        if(userStatus.code === 0) {
            history('/admin/carsCategory')
        }
    }, [userStatus])

    useEffect(() => {
        if(status.code === 0) {
            dispatch(getProfile())
        }
    }, [status])

    const onFinish = (e) => {
        e.preventDefault()
        dispatch(loginStart({phoneNumber: e.target[0].value, password: e.target[1].value}))
    }

    return (
        <div className={'login'}>
            <form onSubmit={onFinish} id={'form-login'}>
                <label htmlFor="i-1" >
                    Phone Number
                </label>
                <input type="text" id='i-1'/>
                <label htmlFor="i-2">
                    Password
                </label>
                <input type="password" id={'i-2'}/>
            </form>
            <div>
                <button type={'submit'} form={'form-login'}>{loading ? "LOADING..." : "Kirish"}</button>
            </div>
        </div>
    );
};

export default Login;