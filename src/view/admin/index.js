import React from 'react';
import Layout from '../../component/Layout'
import Navbar from '../../component/Navbar'

const Index = () => {
    return (
        <div className={'admin'}>
            <Navbar />
            <Layout />
        </div>
    );
};

export default Index;