import React, {useEffect, useState} from 'react';
import './main.css'
import {useDispatch, useSelector} from "react-redux";
import {deleteCar, getCar} from "../../store/car/car";
import {useLocation, useParams} from "react-router-dom";
import {getCategory} from "../../store/category/category";

// import Modal from './Modal'
import Modal from '../Form'
import button from "bootstrap/js/src/button";

const Main = () => {

    const dispatch = useDispatch()
    const {a} = useParams()
    const location = useLocation();

    const {status, data, isLoading, loading, pageCount} = useSelector(state => state.car)
    const {data: dataCategory} = useSelector(state => state.category)

    const [modal, setModal] = useState(false)
    const [update, setUpdate] = useState(false)
    const [pagination, setPagination] = useState(1)

    useEffect(() => {
        if (status.code === 0) {
            debugger
            dispatch(getCar({limit: 5, page: 1, categoryId: a}))
            setUpdate(false);
            setModal(false)
        }
    }, [status])

    useEffect(() => {
        dispatch(getCar({limit: 5, page: pagination, categoryId: a}))
        dispatch(getCategory({limit: 100, page: 1}))
    }, [])


    const updateCar = (item) => {
        setModal(true)
        setUpdate(item)
    }
    const carDelete = (id) => {
        if (window.confirm("REALLY ? ☺")) {
            dispatch(deleteCar(id));
        }
    }
    const openModal = () => {
        setModal(true)
    }

    const onFinish = (e) => {
        e.preventDefault()
    }

    const page = () => {
        let a = Math.ceil(pageCount / 5);
        let data = [];
        for (let i = 1; i <= a; i++) {
            data.push(i)
        }
        return data;
    }

    return (
        <div className={'main'}>
            <div className={'header'}>
                <div>
                    <span/>
                    <h5>Mashinalar</h5>
                </div>
                <button onClick={openModal}>+ Mashina qo'shish</button>
            </div>
            <table className={'footer'}>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Markasi</th>
                    <th>Gearbook</th>
                    <th>Tanirovka</th>
                    <th>Motor</th>
                    <th>Year</th>
                    <th>Color</th>
                    <th>Distance</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {
                    isLoading ? "LOADING..." : data.map((item, index) => <tr key={item._id}>
                        <td>{index + 1}</td>
                        <td>{location.state}</td>
                        <td>{item.gearbok}</td>
                        <td>{item.tonirovka}</td>
                        <td>{item.motor}</td>
                        <td>{item.year}</td>
                        <td>{item.color}</td>
                        <td>{item.distance}</td>
                        <td>
                            <button onClick={() => updateCar(item)} className={'btn-none'}><i
                                className="fa-solid fa-pen"></i></button>
                            <button className={'btn-none'} onClick={() => carDelete(item._id)}><i
                                className="fa-solid fa-trash"></i></button>
                        </td>
                    </tr>)
                }
                </tbody>
            </table>
            {page().map(i =>
                <button style={{padding: "5px 10px", borderRadius: "10px", cursor: "pointer", margin: 5}} onClick={() =>
                    dispatch(getCar({limit: 5, page: i, categoryId: a}))
                }>{i}</button>
            )}
            {modal && <Modal data={update} setModal={setModal} type={true} param={a}/>}
        </div>
    );
};

export default Main;