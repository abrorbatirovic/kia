import React, {useEffect, useState} from 'react';
import {deleteCategory, getCategory} from "../../store/category/category";
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate} from "react-router-dom";
import vector from "../category/Vector.svg";
import button from "bootstrap/js/src/button";
import {getCar} from "../../store/car/car";
import Modal from "../Form";

const Cars = () => {

    const dispatch = useDispatch()
    const history = useNavigate()

    const {status, data, isLoading, loading, pageCount} = useSelector(state => state.category)


    const [modal, setModal] = useState(false)
    const [update, setUpdate] = useState(false)
    const [pagination, setPagination] = useState(1)

    useEffect(() => {
        dispatch(getCategory({limit: 5, page: pagination}))
    }, [])

    useEffect(() => {
        if (status.code === 0) {
            dispatch(getCategory({limit: 5, page: pagination}))
            setUpdate(false);
            setModal(false)
        }
    }, [status])

    const updateCategory = (item) => {
        setModal(true)
        setUpdate(item)
    }
    const categoryDelete = (id) => {
        if (window.confirm("REALLY ? ☺")) {
            dispatch(deleteCategory(id));
        }
    }

    const openModal = () => {
        setModal(true)
    }

    const page = () => {
        let a = Math.ceil(pageCount / 5);
        let data = [];
        for (let i = 1; i <= a; i++) {
            data.push(i)
        }
        return data;
    }

    return (
        <div className={'main'}>
            <div className={'header'}>
                <div>
                    <span/>
                    <h5>Mashinalar</h5>
                </div>
                <button onClick={openModal}>+ Mashina qo'shish</button>
            </div>
            <table className={'footer'}>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Markasi</th>
                    <th/>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {
                    isLoading ? "LOADING..." : data.map((item, index) => <tr key={item._id}>
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>
                            <td>
                                <button onClick={() => updateCategory(item)} className={'btn-none'}><i
                                    className="fa-solid fa-pen"></i></button>
                                <button className={'btn-none'} onClick={() => categoryDelete(item._id)}><i
                                    className="fa-solid fa-trash"></i></button>
                            </td>
                        </td>
                        <td>
                            <Link to={'/admin/' + item._id} state={item.name}><img src={vector} alt={vector}
                                                                                   className={'vector'}/></Link>
                        </td>
                    </tr>)
                }
                </tbody>
            </table>
            {page().map(i =>
                <button style={{padding: "5px 10px", borderRadius: "10px", cursor: "pointer", margin: 5}} onClick={() =>
                    dispatch(getCategory({limit: 5, page: i}))
                }>{i}</button>
            )}
            {modal && <Modal data={update} setModal={setModal} type={false}/>}
        </div>
    );
};

export default Cars;