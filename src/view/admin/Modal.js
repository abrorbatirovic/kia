import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getCategory} from "../../store/category/category";

const Modal = ({data, onFinish, setModal}) => {

    // const dispatch = useDispatch()
    //
    // const {status, data, isLoading, loading, pageCount} = useSelector(state => state.category)
    //
    // useEffect(() => {
    //     dispatch(getCategory({limit: 5, page: 1}))
    // }, [])

    return (
        <div>
            <div className={'modal-card'}>
                <form id={'car'} onSubmit={onFinish} className={'modal'}>
                    <div>
                        <label> Markasi</label>
                        <select defaultValue={'yoq'}>
                            <option value={data[0].marka.name}>{data[0].marka.name}</option>
                        </select>

                        <label> Color</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Gearbook</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Rasm ichki makon</label>
                        <input type="file" placeholder={'Kiriting'}/>

                        <label> Description </label>
                        <textarea placeholder={'Kiriting'}/>


                        <label> Modeli turi uchun rasm</label>
                        <input type="file" placeholder={'Kiriting'}/>

                    </div>
                    <div>
                        <label> Tanirovkasi</label>
                        <select defaultValue={'yoq'}>
                            <option value={'yoq'}>Yoq</option>
                            <option value={'oldi orqa qilingan'}>Oldi orqa qilingan</option>

                        </select>

                        <label> Year</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Distance</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Narxi</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Motor</label>
                        <input type="text" placeholder={'Kiriting'}/>

                        <label> Rasm tashqi makon</label>
                        <input type="file" placeholder={'Kiriting'}/>


                        <div>
                            <button type={'submit'}>Submit</button>
                            <button onClick={() => setModal(false)}>Cancel</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    );
};

export default Modal;