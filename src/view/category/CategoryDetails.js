import React, {useEffect} from 'react';
import vector from "./Vector.svg";
import {Link, useLocation, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getCar} from "../../store/car/car";
import './main.css'
import tahoe from "../../img/car/tahoe.png";
import damas from "../../img/car/damas.png";
import nexia from "../../img/car/nexia.png";
import equinox from "../../img/car/equinox.png";
import photo1 from "../../img/category/Rectangle 1971-1.png";

const CategoryDetails = () => {

    const dispatch = useDispatch();
    const location = useLocation();
    const {categoryId} = useParams();

    const {status, data, isLoading, loading, pageCount} = useSelector(state => state.car)
// debugger
    useEffect(() => {
        dispatch(getCar({limit: 5, page: 1, categoryId}))
    }, [])
    console.log(data,'sc')
    // const dataJson = [
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: tahoe},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: damas},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: equinox},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: nexia},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: photo1},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: photo1},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: photo1},
    //     {_id: '63181666e4b94f1d27e4a21c', name: "tahoe", price: 329900000, img: photo1},
    // ]
    return (
        <div className={'home'}>
            <Link to={'/login'}>
                <div className={'login-btn'}>Login </div>
            </Link>
            <div className={'path'}>
                Bosh sahifa <img src={vector} alt={vector} className={'vector'}/> modellari <img src={vector} alt={vector} className={'vector'}/> {location.state} turlari
            </div>
            <p className={'heading'}>Modellar turlari</p>
            <div className={'row'}>
                {/*{isLoading ? "LOADING..." : data?.map((i, inx) => (*/}
                {isLoading ? "LOADING..." : data?.map((i, inx) => (
                    <div key={i?._id}>
                        <Link to={`/car/${i?._id}`} state={location.state}>
                            {/*<img src={i?.img} alt=""/>*/}
                            <img src={i.imgUrl ? `https://cartestwebapp.herokuapp.com/${i.imgUrl}` : tahoe} alt="No img" width={"100%"} height={"100%"}/>
                            {/*<h5>{location.state.toUpperCase() + " " +  i?.name?.toUpperCase()}</h5>*/}
                            <h5>{location.state.toUpperCase() + " " +  i?.gearbok?.toUpperCase()}</h5>
                            <span>
                                <h4>Narxi: </h4>ᅠ<p> {i?.price}</p>
                            </span>
                        </Link>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default CategoryDetails;