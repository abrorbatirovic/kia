import React, {useEffect} from 'react';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getCategory} from "../../store/category/category";
import './main.css'
import vector from './Vector.svg';
import photo1 from '../../img/category/Rectangle 1971-1.png';
import photo2 from '../../img/category/Rectangle 1971-2.png';
import photo3 from '../../img/category/Rectangle 1971-3.png';
import photo4 from '../../img/category/Rectangle 1971-4.png';
import {clearAuth} from "../../store/auth/auth";

const Category = () => {

    const dispatch = useDispatch();

    const {status, data, isLoading, loading, pageCount} = useSelector(state => state.category)


    useEffect(() => {
        localStorage.clear()
        dispatch(clearAuth())
    }, [])

    useEffect(() => {
        dispatch(getCategory({limit: 100, page: 1}))
    }, [])

    return (
        <div className={'home'}>

            <Link to={'/login'}>
                <div className={'login-btn'}>Login </div>
            </Link>
            <div className={'path'}>
                Bosh sahifa <img src={vector} alt={vector} className={'vector'}/> modellari
            </div>
            <p className={'heading'}>Modellari</p>
            <div className={'row'}>
                {isLoading ? "LOADING..." : data?.map((i, inx)=> (
                    <div key={i?._id}>
                        <Link to={`/category/${i?._id}`} state={i.name}>
                            <img src={"https://cartestwebapp.herokuapp.com/" + i.imgUrl} alt="" width={"100%"} height={"100%"}/>
                            <h5>{i?.name?.toUpperCase()}</h5>
                        </Link>
                    </div>
                    ))}
            </div>
        </div>
    );
};

export default Category;