import React, {Fragment, useEffect} from 'react';
import {Link, Route, Routes, useParams} from "react-router-dom";
import Main from '../../view/admin/Main'
import {useNavigate} from 'react-router-dom'

import './main.css'
import Cars from "../../view/admin/Cars";
import Roles from "../../view/admin/Roles";


const Index = () => {

    const {a} = useParams()

    const history = useNavigate()

    useEffect(() => {
        if(!localStorage.getItem('Authority') && !localStorage.getItem('posToken')) {
            history('/')
        }
    }, [])

    return (
        <Fragment>
            <div className={'layout'}>
                <div className={'layout-top'}>
                    <div className={'nav-stack'}>
                        <Link to={'/admin/carsCategory'} className={'nav-item'}>
                            <span><i className="fa-solid fa-house-user"></i></span>
                            <div>Asosiy</div>
                        </Link>
                        <Link to={'/admin/poster'} className={'nav-item'}>
                            <span><i className="fa-solid fa-store"></i></span>
                            <div>Foydalanuvchi roli</div>
                        </Link>
                        <Link to={'/admin/question'} className={'nav-item'}>
                            <span><i className="fa-solid fa-pen-to-square"></i></span>
                            <div>Savollar</div>
                        </Link>
                    </div>
                </div>
                <div className={'layout-bottom'}>
                    {a === 'carsCategory' ? <Cars /> : a === 'poster' ? <Roles /> : <Main />}
                </div>
            </div>
        </Fragment>
    );
};

export default Index;