import { forwardRef } from 'react'

const input = forwardRef((props, ref) => (
  <select ref={ref} {...props}>
    {props?.options.map((option, index) => (
      <option key={index} value={option.value}>
        {option.label}
      </option>
    ))}
  </select>
))

export default input
