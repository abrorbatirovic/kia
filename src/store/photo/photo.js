import {createSlice} from '@reduxjs/toolkit'
import {
    apiCall,
    apiCar
} from "../config/config";


const photo = createSlice({
    name: 'photo',
    initialState: {
        isLoading: false,
        loading: false,
        data: null,
        oneCar: {},
        status: '',
    },

    reducers: {
        uploadStart: (state) => {
            state.isLoading = false
            state.loading = true
            state.status = ''
            state.data = null
        },
        onFail: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        uploadSuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.data = action.payload.data
        },
    }
})

export const fileUpload = (data) => apiCall({
    url: "upload",
    method: 'post',
    data,
    onStart: photo.actions.uploadStart.type,
    onSuccess: photo.actions.uploadSuccess.type,
    onFail: photo.actions.onFail.type
})

export default photo.reducer
