import {createAction} from '@reduxjs/toolkit'

export const apiCall = createAction('apiCall')

export const apiCategoryList = 'category/marka';
export const apiCategory = 'category';

export const apiCar = 'car';

//auth
export const employeeLogin = 'employee/login'
export const employeeAccount = 'employee/account'


