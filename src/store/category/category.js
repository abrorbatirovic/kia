import {createSlice} from '@reduxjs/toolkit'
import {
    apiCall,
    apiCategoryList,
    apiCategory
} from "../config/config";

const category = createSlice({
    name: 'category',
    initialState: {
        isLoading: false,
        loading: false,
        data: [],
        status: '',
        pageCount: 0,
    },
    reducers: {
        onGetStart: (state) => {
            state.loading = false
            state.isLoading = true
            state.data = []
        },
        onAddStart: (state) => {
            state.isLoading = false
            state.loading = true
        },
        onFail: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        getCategorySuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.data = action.payload.data.data
            state.pageCount = action.payload.data.total
        },
        addCategorySuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        updateCategorySuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        },
        deleteCategorySuccess: (state, action) => {
            state.loading = false
            state.isLoading = false
            state.status = action.payload
        }
    }
})

export const getCategory = (params) => apiCall({
    url: apiCategoryList,
    method: 'get',
    params: params,
    onStart: category.actions.onGetStart.type,
    onSuccess: category.actions.getCategorySuccess.type,
    onFail: category.actions.onFail.type
})

export const addCategory = (data) => apiCall({
    url: apiCategory,
    method: 'post',
    data,
    onStart: category.actions.onAddStart.type,
    onSuccess: category.actions.addCategorySuccess.type,
    onFail: category.actions.onFail.type
})

export const updateCategory = (data) => apiCall({
    url: apiCategory,
    method: 'put',
    data,
    onStart: category.actions.onAddStart.type,
    onSuccess: category.actions.updateCategorySuccess.type,
    onFail: category.actions.onFail.type
})
export const deleteCategory = (id) => apiCall({
    url: apiCategory + "/" + id,
    method: 'delete',
    onStart: category.actions.onGetStart.type,
    onSuccess: category.actions.deleteCategorySuccess.type,
    onFail: category.actions.onFail.type
})

export default category.reducer
