import {configureStore} from '@reduxjs/toolkit'
import api from './middleware/api';
import category from "./category/category";
import car from "./car/car";
import photo from "./photo/photo";
import auth from './auth/auth'

export default configureStore({
    reducer: {
        category,
        car,
        auth,
        photo
    },
    middleware: [api]
})
